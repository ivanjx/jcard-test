package jcardtest;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.Util;

public class JCardTest extends Applet {

    private final static short SW_WRONG_INS = (short)0x6d00;
    
    private byte[] _eepromBuff;
    private byte[] _helloBuff;

    public static void install(byte[] buffer, short offset, byte length) {
        new JCardTest();
    }

    public JCardTest() {
        _eepromBuff = new byte[32];
        _helloBuff = new byte[] {
            0x48, 0x65, 0x6c, 0x6c, 0x6f
        };

        register();
    }

    void hello(APDU apdu, byte[] buffer) {
        // Preparing data.
        Util.arrayCopy(
            _helloBuff,
            (short)0,
            buffer,
            ISO7816.OFFSET_CDATA,
            (short)_helloBuff.length);

        // Sending data.
        apdu.setOutgoingAndSend(
            ISO7816.OFFSET_CDATA, 
            (short)_helloBuff.length);
    }

    void readEEPROM(APDU apdu, byte[] buffer) {
        // Preparing data.
        Util.arrayCopy(
            _eepromBuff,
            (short)0,
            buffer,
            ISO7816.OFFSET_CDATA,
            (short)_eepromBuff.length);
        
        // Sending data.
        apdu.setOutgoingAndSend(
            ISO7816.OFFSET_CDATA,
            (short)_eepromBuff.length);
    }

    void writeEEPROM(APDU apdu, byte[] buffer) {
        // Validating.
        if(apdu.setIncomingAndReceive() != (short)(_eepromBuff.length + 1)) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }

        // Reading and saving data.
        Util.arrayCopy(
            buffer,
            (byte)(ISO7816.OFFSET_CDATA + 1),
            _eepromBuff,
            (short)0,
            (short)_eepromBuff.length);

        // Sending response.
        hello(apdu, buffer);
    }

    public void process(APDU apdu) throws ISOException {
        byte[] buffer = apdu.getBuffer();

        // Check select apdu command.
        if (buffer[ISO7816.OFFSET_CLA] == 0 &&
            buffer[ISO7816.OFFSET_INS] == (byte)0xA4) {
            return;
        }
        
        switch(buffer[ISO7816.OFFSET_CDATA]) {
            case 0x40:
                // Hello command.
                hello(apdu, buffer);
                break;

            case 0x41:
                // Read eeprom command.
                readEEPROM(apdu, buffer);
                break;

            case 0x42:
                // Write eeprom command.
                writeEEPROM(apdu, buffer);
                break;

            default:
                // Unknown command.
                ISOException.throwIt(SW_WRONG_INS);
        }
    }
    
}